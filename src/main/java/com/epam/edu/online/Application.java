package com.epam.edu.online;

import com.epam.edu.online.gson.GsonParser;
import com.epam.edu.online.jackson.JacksonParser;
import com.epam.edu.online.model.Gem;
import com.epam.edu.online.validator.JsonSchemaValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.util.List;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    private static final String SCHEMA_PATH = "src/main/resources/gemsSchema.json";
    private static final String JSON_PATH = "src/main/resources/gems.json";
    private static final String JSON_OBJECT_PATH = "src/main/resources/gem.json";

    public static void main(String[] args) {

        try {
            // Validator
            new JsonSchemaValidator().validate(SCHEMA_PATH, JSON_OBJECT_PATH);

            // Gson
            GsonParser gsonParser = new GsonParser();
            List<Gem> gems1 = gsonParser.parseToObject(JSON_PATH);
            gems1 = gsonParser.sortByOrigin(gems1);
            gems1.forEach(log::info);
            gsonParser.saveObjectToJson(gems1, JSON_PATH);

            // Jackson
            JacksonParser jacksonParser = new JacksonParser();
            List<Gem> gems2 = jacksonParser.parseToObject(JSON_PATH);
            gems2 = jacksonParser.sortByCarat(gems2);
            gems2.forEach(log::info);
            jacksonParser.saveObjectToJson(gems2, JSON_PATH);
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        }
    }
}
