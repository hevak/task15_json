package com.epam.edu.online.model;

public class VisualParams {
    private String color;
    private Integer facets;
    private Integer opacity;

    public VisualParams() {
    }

    public VisualParams(String color, int facets, int opacity) {
        this.color = color;
        this.facets = facets;
        this.opacity = opacity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getFacets() {
        return facets;
    }

    public void setFacets(Integer facets) {
        this.facets = facets;
    }

    public Integer getOpacity() {
        return opacity;
    }

    public void setOpacity(Integer opacity) {
        this.opacity = opacity;
    }

    @Override
    public String toString() {
        return "VisualParams{" +
                "color='" + color + '\'' +
                ", facets=" + facets +
                ", opacity=" + opacity +
                '}';
    }
}
