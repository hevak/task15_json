package com.epam.edu.online.model;

public class Gem {
    private Integer id;
    private String name;
    private Boolean preciousness;
    private String origin;
    private VisualParams visualParams;
    private Double carat;

    public Gem() {
    }

    public Gem(int id, String name, boolean preciousness, String origin, VisualParams visualParams, double carat) {
        this.id = id;
        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.visualParams = visualParams;
        this.carat = carat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(Boolean preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParams getVisualParams() {
        return visualParams;
    }

    public void setVisualParams(VisualParams visualParams) {
        this.visualParams = visualParams;
    }

    public Double getCarat() {
        return carat;
    }

    public void setCarat(Double carat) {
        this.carat = carat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gem gem = (Gem) o;

        if (id != null ? !id.equals(gem.id) : gem.id != null) return false;
        if (name != null ? !name.equals(gem.name) : gem.name != null) return false;
        if (preciousness != null ? !preciousness.equals(gem.preciousness) : gem.preciousness != null) return false;
        if (origin != null ? !origin.equals(gem.origin) : gem.origin != null) return false;
        if (visualParams != null ? !visualParams.equals(gem.visualParams) : gem.visualParams != null) return false;
        return carat != null ? carat.equals(gem.carat) : gem.carat == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (preciousness != null ? preciousness.hashCode() : 0);
        result = 31 * result + (origin != null ? origin.hashCode() : 0);
        result = 31 * result + (visualParams != null ? visualParams.hashCode() : 0);
        result = 31 * result + (carat != null ? carat.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Gem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", preciousness=" + preciousness +
                ", origin='" + origin + '\'' +
                ", visualParams=" + visualParams +
                ", carat=" + carat +
                '}';
    }
}
