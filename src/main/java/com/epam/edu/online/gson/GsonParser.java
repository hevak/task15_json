package com.epam.edu.online.gson;

import com.epam.edu.online.model.Gem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GsonParser {
    private static final Logger log = LogManager.getLogger(GsonParser.class);

    public void saveObjectToJson(List<Gem> gems, String jsonPath){
        try (Writer writer = new FileWriter(jsonPath)) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(gems, writer);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public List<Gem> parseToObject(String jsonPath) {
        Gson gson = new Gson();
        List<Gem> gems = null;
        try (FileReader json = new FileReader(jsonPath)) {
            gems = Arrays.asList(gson.fromJson(json, Gem[].class));
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return gems;
    }

    public List<Gem> sortByOrigin(List<Gem> gem) {
        return gem.stream().sorted(Comparator.comparing(Gem::getOrigin)).collect(Collectors.toList());
    }

}
