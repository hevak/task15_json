package com.epam.edu.online.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class JsonSchemaValidator {
    private static final Logger log = LogManager.getLogger(JsonSchemaValidator.class);
    public void validate(String schemaPath, String jsonPath) throws FileNotFoundException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileInputStream(schemaPath)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileInputStream(jsonPath)));

        Schema schema = SchemaLoader.load(jsonSchema);
        schema.validate(jsonSubject);
        log.info("json valid");
    }
}
