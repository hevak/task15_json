package com.epam.edu.online.jackson;

import com.epam.edu.online.gson.GsonParser;
import com.epam.edu.online.model.Gem;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class JacksonParser {
    private static final Logger log = LogManager.getLogger(GsonParser.class);
    private ObjectMapper mapper = new ObjectMapper();

    public List<Gem> parseToObject(String jsonPath) {
        List<Gem> gems = null;
        try (FileReader json = new FileReader(jsonPath)) {
            gems = Arrays.asList(mapper.readValue(json, Gem[].class));
        } catch (FileNotFoundException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return gems;
    }

    public List<Gem> sortByCarat(List<Gem> gem) {
        return gem.stream().sorted(Comparator.comparing(Gem::getCarat)).collect(Collectors.toList());
    }

    public void saveObjectToJson(List<Gem> gems, String jsonPath) {
        try (Writer writer = new FileWriter(jsonPath)) {
            String arrayToJson = mapper.writeValueAsString(gems);
            writer.write(arrayToJson);
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
